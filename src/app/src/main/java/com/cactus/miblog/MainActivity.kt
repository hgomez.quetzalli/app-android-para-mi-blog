package com.cactus.miblog

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.ReportFragment.Companion.reportFragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.lottie.LottieCompositionFactory
import com.cactus.miblog.api.ApiResponseStatus
import com.cactus.miblog.databinding.ActivityMainBinding
import com.cactus.miblog.databinding.FragmentEnlacesBinding
import com.cactus.miblog.databinding.FragmentPublicacionesBinding
import com.cactus.miblog.enlaces.EnlaceViewModel
import com.cactus.miblog.enlaces.EnlacesFragment
import com.cactus.miblog.muestraPublicacion.PublicacionFragment
import com.cactus.miblog.muestraTodasPublicaciones.PublicacionesFragment
import com.cactus.miblog.muestraTodasPublicaciones.PublicacionesViewModel
import com.cactus.miblog.muestraTodasPublicaciones.listView.PublicacionAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), PublicacionesFragment.PublicacionesFragmentActions, EnlacesFragment.EnlacesFragmentActions{

    private val viewModel : PublicacionesViewModel by viewModels()
    private val viewModelEnlaces : EnlaceViewModel by viewModels()
    private lateinit var binding: FragmentPublicacionesBinding
    private lateinit var bindingEnlace : FragmentEnlacesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        enableEdgeToEdge()
        setContentView(binding.root)

        val toolbar: Toolbar = binding.appBarLayout
        setSupportActionBar(toolbar);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        viewModel.status.observe(this){
                status ->

            when(status){
                is ApiResponseStatus.Error -> {
                    //parar progress bar
                    Log.d(TAG, "error")
                    val lottieJsonRawResourceId = R.raw.error
                    LottieCompositionFactory.fromRawRes(applicationContext, lottieJsonRawResourceId)
                        .addListener { composition ->
                            // Comprobar si la composición se cargó correctamente
                            if (composition != null) {
                                // Establecer la animación en la vista LottieAnimationView
                                binding.idLottie.setComposition(composition)
                                // Iniciar la animación
                                binding.idLottie.playAnimation()
                            } else {

                                Toast.makeText(applicationContext, "no se cargo el archivo de error", Toast.LENGTH_SHORT).show()
                            }
                        }
                }
                is ApiResponseStatus.Loading -> {
                    //mostrar progress bar
                    Log.d(TAG, "loading")
                    binding.idLottie.visibility = View.VISIBLE
                }
                is ApiResponseStatus.Success -> {
                    //parar progress bar
                    Log.d(TAG, "success")
                }
            }
        }

        viewModel.publicacionesLista.observe(this){
            lista ->
            if(!lista.isNullOrEmpty()){

                binding.idLottie.visibility = View.GONE
                binding.idFragment.visibility = View.VISIBLE

                val recycler = this.binding.publicacionesRecycler
                recycler.layoutManager = GridLayoutManager(this.binding.publicacionesRecycler.context, 1)
                val adapter = PublicacionAdapter()
                adapter.setOnItemClickListener {

                    val bundle = Bundle().apply {
                        putSerializable("publicacion", it)
                    }

                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_PublicacionesFragment_to_publicacionFragment, bundle)

                }
                adapter.submitList(lista)
                recycler.adapter = adapter
            }else{

                binding.idLottie.visibility = View.VISIBLE
                binding.idFragment.visibility = View.GONE

                val lottieJsonRawResourceId = R.raw.vacio
                LottieCompositionFactory.fromRawRes(applicationContext, lottieJsonRawResourceId)
                    .addListener { composition ->
                        // Comprobar si la composición se cargó correctamente
                        if (composition != null) {
                            // Establecer la animación en la vista LottieAnimationView
                            binding.idLottie.setComposition(composition)
                            // Iniciar la animación
                            binding.idLottie.playAnimation()
                        } else {
                            Toast.makeText(applicationContext, "no se cargo el archivo de error", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }

        viewModelEnlaces.enlaceLista.observe(this){
            listaEnlaces ->
            this.bindingEnlace.idLottieLinkedIn.setOnClickListener(View.OnClickListener {
                if(!listaEnlaces.isNullOrEmpty()){
                    if(listaEnlaces[0].titulo == "linkedin"){
                        val linkedInProfileUrl = listaEnlaces[0].url
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(linkedInProfileUrl))
                        intent.setPackage("com.linkedin.android") // Esto abre la aplicación de LinkedIn si está instalada

                        try {
                            startActivity(intent)
                        } catch (e: ActivityNotFoundException) {
                            // Si la aplicación de LinkedIn no está instalada, abre el navegador web
                            intent.setPackage(null)
                            startActivity(intent)
                        }
                    }
                }
            })
        }
    }

    override fun getTodasPublicaciones(binding: FragmentPublicacionesBinding) {
        this.binding = binding
        viewModel.getTodasPublicaciones()
    }

    override fun getEnlaces(binding: FragmentEnlacesBinding) {
        this.bindingEnlace = binding
        viewModelEnlaces.getEnlaces()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_pp, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {

            R.id.submenu_principal -> {

                val bundle = Bundle().apply {
                }
                findNavController(R.id.nav_host_fragment).navigate(R.id.action_go_to_publicacionesFragment, bundle)
                true
            }
            R.id.submenu_enlaces -> {
                // Manejar la acción del elemento de menú "submenu_enlaces"
                val bundle = Bundle().apply {
                }
                findNavController(R.id.nav_host_fragment).navigate(R.id.action_PublicacionesFragment_to_enlacesFragment, bundle)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}