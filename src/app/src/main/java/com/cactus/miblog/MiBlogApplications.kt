package com.cactus.miblog

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MiBlogApplications: Application()