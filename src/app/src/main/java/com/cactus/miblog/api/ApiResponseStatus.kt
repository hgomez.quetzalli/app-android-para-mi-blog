package com.cactus.miblog.api


/**
 * Las sealed class en Kotlin son aquellas empleadas para construir una herencia cerrada en la que
 * el compilador conoce cuáles son las únicas clases hijas, ya que no puede haber otras.
 * La principal limitación de los enum es que todos los subtipos siguen una misma estructura.
 * Por ejemplo, en el caso de los planetas todos tenían un radio, un método para obtener su densidad,
 * otro para obtener su nombre, etc. Sin embargo, cada subtipo (subclase) de una sealed class es una
 * clase que puede ser de la manera que quiera, con sus propios valores y métodos.
 *
 * Se agrega la <T> para que el valor que regrese sea generico y no solo pueda regresar una lista de perros
 */
sealed class ApiResponseStatus<T>() {
    class Success<T>(val data : T) : ApiResponseStatus<T>()
    class Loading<T>() : ApiResponseStatus<T>()
    class Error<T>(val messageId : Int) : ApiResponseStatus<T>()
}