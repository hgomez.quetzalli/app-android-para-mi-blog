package com.cactus.miblog.api
import com.cactus.miblog.GET_ENLACES
import retrofit2.http.*
import com.cactus.miblog.GET_TODAS_PUBLICACIONES
import com.cactus.miblog.GET_TODAS_PUBLICACIONES_IMAGENES
import com.cactus.miblog.model.Enlace
import com.cactus.miblog.model.EnlaceDomain
import com.cactus.miblog.model.Publicacion
import com.cactus.miblog.model.PublicacionImagen

/**
 * interface que tendra funciones que representan las llamadas a los servicios web
 */
interface ApiService{

    @GET(GET_TODAS_PUBLICACIONES)
    suspend fun getTodasPublicaciones(@Header("Authorization") auth: String ) : List<Publicacion>

    @GET(GET_TODAS_PUBLICACIONES_IMAGENES)
    suspend fun getTodasPublicacionesImagenes(@Header("Authorization") auth: String ) : List<PublicacionImagen>

    @GET(GET_ENLACES)
    suspend fun getEnlaces(@Header("Authorization") auth: String ) : List<Enlace>

}