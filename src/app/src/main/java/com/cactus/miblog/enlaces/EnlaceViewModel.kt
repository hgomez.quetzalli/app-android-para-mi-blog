package com.cactus.miblog.enlaces

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cactus.miblog.TAG
import com.cactus.miblog.api.ApiResponseStatus
import com.cactus.miblog.model.EnlaceDomain
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EnlaceViewModel @Inject constructor(private val enlacesRepository: EnlacesTask): ViewModel(){

    private val _enlacesLista = MutableLiveData<List<EnlaceDomain>>()
    val enlaceLista: LiveData<List<EnlaceDomain>>
        get() = _enlacesLista

    private val _status = MutableLiveData<ApiResponseStatus<Any>>()
    val status : LiveData<ApiResponseStatus<Any>>
        get() = _status

    fun getEnlaces(){
        viewModelScope.launch {
            _status.value = ApiResponseStatus.Loading()
            hadleResponseStatus(enlacesRepository.getEnlaces())
        }
    }

    private fun hadleResponseStatus(apiResponseStatus: ApiResponseStatus<List<EnlaceDomain>>){
        if(apiResponseStatus is ApiResponseStatus.Success){
            _enlacesLista.value = apiResponseStatus.data
        }else if(apiResponseStatus is ApiResponseStatus.Loading){
            Log.d(TAG, "mainviewmodel handle loading")
        }else if(apiResponseStatus is ApiResponseStatus.Error){
            Log.d(TAG, "mainviewmodel handle Error")
        }
        _status.value = apiResponseStatus as ApiResponseStatus<Any>
    }
}