package com.cactus.miblog.enlaces

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cactus.miblog.R
import com.cactus.miblog.databinding.FragmentEnlacesBinding


class EnlacesFragment : Fragment() {

    private lateinit var binding: FragmentEnlacesBinding

    interface EnlacesFragmentActions{
        fun getEnlaces(binding: FragmentEnlacesBinding)
    }

    lateinit var enlacesFragmentActions: EnlacesFragmentActions

    override fun onAttach(context: Context) {
        super.onAttach(context)
        enlacesFragmentActions = try {
            context as EnlacesFragmentActions
        }catch (e : ClassCastException){
            throw ClassCastException("$context must implement EnlaceFragmentActions")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEnlacesBinding.inflate(inflater)
        enlacesFragmentActions.getEnlaces(binding)

        return binding.root
    }

}