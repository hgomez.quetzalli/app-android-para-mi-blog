package com.cactus.miblog.enlaces

import android.util.Log
import com.cactus.miblog.R
import com.cactus.miblog.TAG
import com.cactus.miblog.api.ApiResponseStatus
import com.cactus.miblog.api.ApiService
import com.cactus.miblog.api.makeNetworkCall
import com.cactus.miblog.model.Enlace
import com.cactus.miblog.model.EnlaceDTOMapper
import com.cactus.miblog.model.EnlaceDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface EnlacesTask {
    suspend fun getEnlaces(): ApiResponseStatus<List<EnlaceDomain>>
}

class EnlacesRepository @Inject constructor(private val apiService: ApiService, private val dispatcher: CoroutineDispatcher): EnlacesTask{

    override suspend fun getEnlaces(): ApiResponseStatus<List<EnlaceDomain>> {
        return withContext(dispatcher){
            when(val enlaces: ApiResponseStatus<List<EnlaceDomain>> = getEnlacesPrivate()){
                is ApiResponseStatus.Error -> {
                    enlaces
                }
                is ApiResponseStatus.Success -> {
                    Log.d(TAG, enlaces.data.toString())
                    ApiResponseStatus.Success(enlaces.data)
                }else -> {
                    ApiResponseStatus.Error(R.string.unknown_error)
                }
            }
        }
    }

    private suspend fun getEnlacesPrivate(): ApiResponseStatus<List<EnlaceDomain>> {
        return makeNetworkCall {

            val authHeader:String = "Basic bXl1c2VybmFtZTpwYXNzd29yZDEyMw=="
            val listEnlace = apiService.getEnlaces(authHeader)
            val enlaceDTOMapper = EnlaceDTOMapper()
            enlaceDTOMapper.fromEnlaceDTOListToEnlaceDomainList(listEnlace)
        }
    }

}