package com.cactus.miblog.id

import com.cactus.miblog.enlaces.EnlacesRepository
import com.cactus.miblog.enlaces.EnlacesTask
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class EnlacesTaskModule {

    @Binds
    abstract fun bindEnlaceTasks(enlacesRepository: EnlacesRepository): EnlacesTask

}