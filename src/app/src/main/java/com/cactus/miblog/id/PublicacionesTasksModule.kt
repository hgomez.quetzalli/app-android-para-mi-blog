package com.cactus.miblog.id

import com.cactus.miblog.muestraTodasPublicaciones.PublicacionesRepository
import com.cactus.miblog.muestraTodasPublicaciones.PublicacionesTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class PublicacionesTasksModule {

    @Binds
    abstract fun bindAuthTasks(publicacionesRepository: PublicacionesRepository): PublicacionesTasks
}