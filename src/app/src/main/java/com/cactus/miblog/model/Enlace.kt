package com.cactus.miblog.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
class Enlace (
    @field:Json(name = "id")val id: Int,
    @field:Json(name = "titulo")val titulo: String,
    @field:Json(name = "url")val url: String,
): Parcelable