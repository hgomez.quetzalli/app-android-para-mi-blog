package com.cactus.miblog.model

class EnlaceDTOMapper {

    fun fromEnlaceDTOToEnlaceDomain(enlace: Enlace): EnlaceDomain{
        return EnlaceDomain(enlace.id, enlace.titulo, enlace.url)
    }

    fun fromEnlaceDTOListToEnlaceDomainList(enlaceList: List<Enlace>): List<EnlaceDomain>{
        return enlaceList.map { fromEnlaceDTOToEnlaceDomain(it) }
    }

}