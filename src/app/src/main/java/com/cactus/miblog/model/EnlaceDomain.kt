package com.cactus.miblog.model

import java.io.Serializable

class EnlaceDomain (
    val id: Int,
    val titulo: String,
    val url: String,
): Serializable