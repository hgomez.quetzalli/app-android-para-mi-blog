package com.cactus.miblog.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
class Imagen(
    @field:Json(name = "id")val id: Int,
    @field:Json(name = "url")val url: String,
    @field:Json(name = "descripcion")var descripcion:String,
    @field:Json(name = "contenido")var contenido:String,
    @field:Json(name = "fecha_subida")var fechaSubida:String): Parcelable