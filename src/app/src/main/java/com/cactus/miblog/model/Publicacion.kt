package com.cactus.miblog.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
class Publicacion(
    @field:Json(name = "id")val id: Int,
    @field:Json(name = "titulo")val titulo: String,
    @field:Json(name = "subtitulo")var subtitulo:String,
    @field:Json(name = "contenido")var contenido:String,
    @field:Json(name = "fecha_creacion")var fechaCreacion:String,
    var categoria:String,
    var urlImagen:String): Parcelable