package com.cactus.miblog.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
class PublicacionImagen(
    @field:Json(name = "id")val id: Int,
    @field:Json(name = "publicacion")val publicacion: Publicacion,
    @field:Json(name = "imagen")val imagen: Imagen): Parcelable