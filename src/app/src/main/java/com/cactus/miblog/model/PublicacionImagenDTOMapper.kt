package com.cactus.miblog.model

class PublicacionImagenDTOMapper {

    /**
     * funcion que toma un dogDto y lo transforma a un dog
     */
    fun fromPubImaDTOToPubImaDomain(pubImaDTO: PublicacionImagen): PublicacionImagenDomain {
        return PublicacionImagenDomain(pubImaDTO.id, pubImaDTO.publicacion, pubImaDTO.imagen)
    }

    /**
     * funcion que toma una lista de dogDTO y lo transforma a una lista de dog
     */
    fun fromPubImaDTOListToPubImaDomainList(pubImaDTOList: List<PublicacionImagen>) : List<PublicacionImagenDomain>{
        return pubImaDTOList.map { fromPubImaDTOToPubImaDomain(it) }
    }

}