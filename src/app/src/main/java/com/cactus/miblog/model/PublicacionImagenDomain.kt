package com.cactus.miblog.model

import java.io.Serializable

class PublicacionImagenDomain(
    val id: Int,
    val publicacion: Publicacion,
    val imagen: Imagen): Serializable