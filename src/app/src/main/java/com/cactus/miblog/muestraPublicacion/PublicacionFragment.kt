package com.cactus.miblog.muestraPublicacion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cactus.miblog.R
import com.cactus.miblog.databinding.FragmentPublicacionBinding
import com.cactus.miblog.model.PublicacionImagenDomain
import com.squareup.picasso.Picasso

class PublicacionFragment: Fragment()  {

    private lateinit var binding: FragmentPublicacionBinding
    private lateinit var publicacionImagenDomain:PublicacionImagenDomain

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentPublicacionBinding.inflate(inflater)

        arguments?.let {
            publicacionImagenDomain = it.getSerializable("publicacion") as PublicacionImagenDomain
        }

        binding.idTituloPublicacion.text = publicacionImagenDomain.publicacion.titulo
        binding.idFechaPublicacion.text = publicacionImagenDomain.publicacion.fechaCreacion
        Picasso.get()
            .load(publicacionImagenDomain.imagen.url)
            .placeholder( R.drawable.ic_menu_24dp)
            .error(R.drawable.ic_menu_24dp)
            .into(binding.idImageView);
        binding.textView.text = publicacionImagenDomain.publicacion.contenido

        //Toast.makeText(context, publicacionImagenDomain.publicacion.titulo, Toast.LENGTH_SHORT).show()
        //publicacionesFragmentActions.getTodasPublicaciones(binding)
        return binding.root
    }

}