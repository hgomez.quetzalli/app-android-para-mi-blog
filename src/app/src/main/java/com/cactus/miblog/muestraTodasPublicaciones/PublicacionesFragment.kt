package com.cactus.miblog.muestraTodasPublicaciones

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.cactus.miblog.databinding.AdapterListPublicacionesBinding
import com.cactus.miblog.databinding.FragmentPublicacionesBinding
import com.cactus.miblog.model.Publicacion
import com.cactus.miblog.muestraTodasPublicaciones.listView.PublicacionAdapter

class PublicacionesFragment : Fragment() {

    private lateinit var binding: FragmentPublicacionesBinding

    interface PublicacionesFragmentActions{
        fun getTodasPublicaciones(binding: FragmentPublicacionesBinding)

    }

    lateinit var publicacionesFragmentActions : PublicacionesFragmentActions

    override fun onAttach(context: Context) {
        super.onAttach(context)
        publicacionesFragmentActions = try{
            context as PublicacionesFragmentActions
        }catch (e : ClassCastException){
            throw ClassCastException("$context must implement LoginFragmentActions")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentPublicacionesBinding.inflate(inflater)
        publicacionesFragmentActions.getTodasPublicaciones(binding)


        return binding.root
    }

    private fun getListPublicaciones(): List<Publicacion> {
        var listaPublicaciones = mutableListOf<Publicacion>()
        listaPublicaciones.add(Publicacion(1, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(2, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(3, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(4, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(5, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(6, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(7, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(8, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(9, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))
        listaPublicaciones.add(Publicacion(10, "Digital Revolution", "", "As technology " +
                "continues to evolve at a rapid pace, it's crucial to stay up-to-date with the latest " +
                "trends and innovations shaping our digital landscape. In this tech blog, we'll explore " +
                "some of the most exciting developments across various domains, from artificial intelligence " +
                "and blockchain to augmented reality and quantum computing. Join us on this journey into " +
                "the future as we delve into the fascinating world of technology.", "13/febrero/1994",
            "IA", "https://images.dog.ceo/breeds/dhole/n02115913_612.jpg"))

        return listaPublicaciones
    }
}