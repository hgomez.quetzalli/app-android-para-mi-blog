package com.cactus.miblog.muestraTodasPublicaciones

import android.util.Log
import com.cactus.miblog.R
import com.cactus.miblog.api.ApiResponseStatus
import com.cactus.miblog.api.ApiService
import com.cactus.miblog.api.makeNetworkCall
import com.cactus.miblog.model.Publicacion
import com.cactus.miblog.model.PublicacionImagen
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject
import com.cactus.miblog.TAG
import com.cactus.miblog.model.PublicacionImagenDTOMapper
import com.cactus.miblog.model.PublicacionImagenDomain

interface PublicacionesTasks {
    suspend fun getTodasPublicaciones(): ApiResponseStatus<List<Publicacion>>
    suspend fun getTodasPublicacionesImagenes(): ApiResponseStatus<List<PublicacionImagenDomain>>
}

class PublicacionesRepository @Inject constructor(private val apiService: ApiService, private val dispatcher: CoroutineDispatcher) : PublicacionesTasks{

    override suspend fun getTodasPublicaciones(): ApiResponseStatus<List<Publicacion>> {

        return withContext(dispatcher){
            when (val allPublicaciones: ApiResponseStatus<List<Publicacion>> = getTodasPublicacionesPrivate()) {
                is ApiResponseStatus.Error -> {
                    allPublicaciones
                }
                is ApiResponseStatus.Success -> {
                    Log.d(TAG, allPublicaciones.data.toString())
                    ApiResponseStatus.Success(allPublicaciones.data)
                }
                else -> {
                    ApiResponseStatus.Error(R.string.unknown_error)
                }
            }
        }
    }

    private suspend fun getTodasPublicacionesPrivate(): ApiResponseStatus<List<Publicacion>> {
        return makeNetworkCall {

            val authHeader:String = "Basic bXl1c2VybmFtZTpwYXNzd29yZDEyMw=="
            val listPublicaciones = apiService.getTodasPublicaciones(authHeader)
            listPublicaciones
        }
    }

    override suspend fun getTodasPublicacionesImagenes(): ApiResponseStatus<List<PublicacionImagenDomain>> {

        val lista : ApiResponseStatus<List<PublicacionImagenDomain>> = getTodasPublicacionesImagenesPrivate()
        Log.d(TAG, lista.toString())

        return withContext(dispatcher){
            when (val allPublicaciones: ApiResponseStatus<List<PublicacionImagenDomain>> = getTodasPublicacionesImagenesPrivate()) {
                is ApiResponseStatus.Error -> {
                    allPublicaciones
                }
                is ApiResponseStatus.Success -> {
                    Log.d(TAG, allPublicaciones.data.toString())
                    ApiResponseStatus.Success(allPublicaciones.data)
                }
                else -> {
                    ApiResponseStatus.Error(R.string.unknown_error)
                }
            }
        }
    }

    private suspend fun getTodasPublicacionesImagenesPrivate(): ApiResponseStatus<List<PublicacionImagenDomain>> {
        return makeNetworkCall {
            val authHeader:String = "Basic bXl1c2VybmFtZTpwYXNzd29yZDEyMw=="
            val response = apiService.getTodasPublicacionesImagenes(authHeader)
            val pubImaMapper = PublicacionImagenDTOMapper()
            pubImaMapper.fromPubImaDTOListToPubImaDomainList(response)
        }
    }
}