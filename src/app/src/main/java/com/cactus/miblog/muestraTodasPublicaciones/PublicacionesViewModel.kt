package com.cactus.miblog.muestraTodasPublicaciones

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cactus.miblog.api.ApiResponseStatus
import com.cactus.miblog.TAG
import com.cactus.miblog.model.PublicacionImagen
import com.cactus.miblog.model.PublicacionImagenDomain
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PublicacionesViewModel @Inject constructor(private val publicacionesRepo: PublicacionesTasks): ViewModel() {

    /**
     * we share the liveData with the MainActivity, it has a map where the key is the country and
     * the value is the country code
     */
    private val _publicacionesLista = MutableLiveData<List<PublicacionImagenDomain>?>()
    val publicacionesLista : LiveData<List<PublicacionImagenDomain>?>
        get() = _publicacionesLista

    /**
     * we are going to use this liveData to the progressBar
     */
    private val _status = MutableLiveData<ApiResponseStatus<Any>>()
    val status : LiveData<ApiResponseStatus<Any>>
        get() = _status

    fun getTodasPublicaciones(){
        viewModelScope.launch {
            _status.value = ApiResponseStatus.Loading()
            handleResponseStatus(publicacionesRepo.getTodasPublicacionesImagenes())
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<List<PublicacionImagenDomain>>){
        if(apiResponseStatus is ApiResponseStatus.Success){
            _publicacionesLista.value = apiResponseStatus.data
        }else if(apiResponseStatus is ApiResponseStatus.Loading){
            Log.d(TAG, "mainviewmodel handle loading")
        }else if(apiResponseStatus is ApiResponseStatus.Error){
            Log.d(TAG, "mainviewmodel handle error")
        }
        _status.value = apiResponseStatus as ApiResponseStatus<Any>
    }
}