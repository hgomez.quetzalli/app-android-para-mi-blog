package com.cactus.miblog.muestraTodasPublicaciones.listView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cactus.miblog.R
import com.cactus.miblog.databinding.AdapterListPublicacionesBinding
import com.cactus.miblog.model.Publicacion
import com.cactus.miblog.model.PublicacionImagen
import com.cactus.miblog.model.PublicacionImagenDomain
import com.squareup.picasso.Picasso

class PublicacionAdapter : ListAdapter<PublicacionImagenDomain, PublicacionAdapter.PublicacionViewHolder>(DiffCallback)  {

    /**
     * class compare two lists and calculate the differences between them.
     * It is often used in conjunction with RecyclerView to efficiently update the UI when data changes.
     */
    companion object DiffCallback : DiffUtil.ItemCallback<PublicacionImagenDomain>() {

        override fun areItemsTheSame(oldItem: PublicacionImagenDomain, newItem: PublicacionImagenDomain): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PublicacionImagenDomain, newItem: PublicacionImagenDomain): Boolean {
            return oldItem.id == newItem.id
        }
    }

    /**
     * we create a new viewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PublicacionViewHolder {
        val binding = AdapterListPublicacionesBinding.inflate(LayoutInflater.from(parent.context))
        return PublicacionViewHolder(binding)
    }

    /**
     * We binding the data to a viewHolder
     */
    override fun onBindViewHolder(currencyViewHolder: PublicacionViewHolder, position: Int) {
        val currency = getItem(position)
        currencyViewHolder.bind(currency)
    }

    /**
     * this property that is a function given from MainActivity
     */
    private var onItemClickListener: ((PublicacionImagenDomain) -> Unit)? = null
    fun setOnItemClickListener(onItemClickListener: (PublicacionImagenDomain) -> Unit){
        this.onItemClickListener = onItemClickListener
    }

    /**
     * ViewHolder that gets an adapterListCurrencyBinding to bind the currency data with the adapterListCurrency views
     */
    inner class PublicacionViewHolder(private val binding: AdapterListPublicacionesBinding) : RecyclerView.ViewHolder(binding.root){

        /**
         * Function to bind currency data with the adapterListCurrency views
         */
        fun bind(publicacion: PublicacionImagenDomain){
            binding.idTituloPublicacion.text = publicacion.publicacion.titulo
            binding.idContenidoPublicacion.text = publicacion.publicacion.contenido
            binding.idFechaPublicacion.text = publicacion.publicacion.fechaCreacion
            Picasso.get()
                .load(publicacion.imagen.url)
                .placeholder( R.drawable.ic_menu_24dp)
                .error(R.drawable.ic_menu_24dp)
                .into(binding.idImageView);
            binding.idRecyclerPublicaciones.setOnClickListener {
                onItemClickListener?.invoke(publicacion)
            }
        }
    }
}